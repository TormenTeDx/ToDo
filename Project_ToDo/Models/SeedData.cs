﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Project_ToDo.Data;
using System;
using System.Linq;

namespace Project_ToDo.Models
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new ItemContext(
                serviceProvider.GetRequiredService<
                    DbContextOptions<ItemContext>>()))
            {
                if (context.Item.Any())
                {
                    return;   // DB has been seeded
                }

                context.Item.AddRange(
                    new Item
                    {
                        Name = "Matematyka",
                        CreateDate = DateTime.Parse("2021-3-23"),
                        Description = "Odrobić zadanie domowe z matematyki",
                        Priority = 1,
                        Duration = 30
                    },

                    new Item
                    {
                        Name = "Zakupy",
                        CreateDate = DateTime.Parse("2021-3-23"),
                        Description = "Zrobić zakupy",
                        Priority = 2,
                        Duration = 120
                    },

                    new Item
                    {
                        Name = "Mieszkanie",
                        CreateDate = DateTime.Parse("2021-3-22"),
                        Description = "Sprzątnąć mieszkanie",
                        Priority = 1,
                        Duration = 60
                    }
                ); ;
                context.SaveChanges();
            }
        }
    }
}