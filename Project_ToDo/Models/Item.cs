﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Project_ToDo.Models
{
    public class Item
    {
        public int ID { get; set; }
        [StringLength(30)]
        [Required]
        public String Name { get; set; }
        [Display(Name = "Date")]
        [DataType(DataType.Date)]
        [Required]
        public DateTime CreateDate { get; set; }
        [StringLength(200)]
        public String Description { get; set; }
        [Range(1, 5, ErrorMessage = "This field should have values 1-5")]
        [Display(Name = "Priority (1-5)")]
        [Required]
        public short Priority { get; set; }
        [Display(Name = "Duration (minutes)")]
        public int Duration { get; set; }
    }
}
